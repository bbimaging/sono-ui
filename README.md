# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Docker commands

Build container:
```
docker build -t davidjbarnes/bbimaging-sono-ui .
```

Run container:
```
docker run --name sono-ui -p 3000:3000 --rm -d davidjbarnes/bbimaging-sono-u
```