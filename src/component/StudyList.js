import React from 'react';

class StudyList extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
            studies: []
		};
	}

	componentDidUpdate(prevProps) {
		if (prevProps.studies !== this.props.studies) {
			this.setState({studies:this.props.studies});
		}
	}
	
	render() {
		const { studies } = this.state;

        if(studies.length === 0) { return(<div>Loading...</div>) }

		return (
            <ul>
                {studies.map(study => (
                    <li key={study._id} onClick={() => this.props.loadFrames(study._id) }>
                        {study._id}
                    </li>
                ))}
            </ul>
        );
	}
}

export default StudyList;
