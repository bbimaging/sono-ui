import React from 'react';

class ThumbnailViewer extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
            frames: []
		};
	}

	componentDidUpdate(prevProps) {
		if (prevProps.frames !== this.props.frames) {
			this.setState({frames:this.props.frames});
		}
	}

	render() {
		const { frames } = this.state;

		return (
            <ul>
                {frames.map(frame => (
                    <img src={frame.url} style={{ width:'200px' }} key={frame._id} onClick={() => this.props.onChange(frame)} />
                ))}
            </ul>
        );
	}
}

export default ThumbnailViewer;
