import React from 'react';

class FrameViewer extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
            frame: null
		};
	}

	componentDidUpdate(prevProps) {
		if (prevProps.frame !== this.props.frame) {
			this.setState({frame:this.props.frame});
		}
	}

	render() {
		const { frame } = this.state;
		if(frame) {
			return (
				<div>
					<img src={frame.url} />
					<div>
						<p>{frame.createdAt}</p>
						<p>{frame.description}</p>
					</div>
				</div>
			);
		}
		
		return (<div></div>);
	}
}

export default FrameViewer;
