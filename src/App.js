import React from 'react';
import FrameViewer from './component/FrameViewer';
import StudyList from './component/StudyList';
import './app.css'
import ThumbnailViewer from './component/ThumbnailViewer';

class App extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			studies: [],
			frames: [],
			frame: null
		};
	}

	componentDidMount() {
		fetch("http://localhost:8080/study").then(res => res.json())
			.then((studies) => {
				this.setState({ studies });
			}, (error) => {
				this.setState({
					isLoaded: true,
					error
				});
			}
			)
	}

	loadFrames(studyId) {
		fetch("http://localhost:8080/frame/study/" + studyId).then(res => res.json())
			.then((frames) => {
				this.setState({ frames });
			}, (error) => {
				this.setState({
					error
				});
			}
		);
	}

	render() {
		const { studies, frames, frame } = this.state;
		console.log(frames);
		return (
			<div className="container">
				<div className="left">
					<StudyList studies={studies} loadFrames={(studyId) => this.loadFrames(studyId)} />
					<ThumbnailViewer frames={frames} onChange={(frame) => this.setState({frame})} />
				</div>
				<div className="right">
					<FrameViewer frame={frame} />
				</div>
			</div>
		);
	}
}

export default App;
